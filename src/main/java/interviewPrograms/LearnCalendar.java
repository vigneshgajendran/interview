package interviewPrograms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnCalendar {

	public static void main(String[] args) throws ParseException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://leafground.com");
		driver.findElementByXPath("//img[@alt='Calendar']").click();
		driver.findElementById("datepicker").click();

		// Date to be Selected
		String expectedDate = "10/01/2019"; // DD/MM/YYYY

		// Split the expected date to be selected
		String[] splitDate = expectedDate.split("/");

		// Getting the month from the calendar
		int monthToSelect = Integer.parseInt(splitDate[1]);

		// Find the month in the calendar
		String selectedMonth = driver.findElementByClassName("ui-datepicker-month").getText();
		System.out.println(selectedMonth);

		// Set format of the calendar as in application (Input) - Month Text
		SimpleDateFormat inputFormat = new SimpleDateFormat("MMMM");
		Calendar cal = Calendar.getInstance();
		cal.setTime(inputFormat.parse(selectedMonth));

		// Convert the input month format(Text) to month number
		SimpleDateFormat outputFormat = new SimpleDateFormat("MM");
		System.out.println(outputFormat.format(cal.getTime()));
		int presentMonth = Integer.parseInt(outputFormat.format(cal.getTime()));
		System.out.println(presentMonth);

		// Selecting the Date
		if (monthToSelect > presentMonth) {
			for (int i = 0; i < monthToSelect - presentMonth; i++) {
				driver.findElementByXPath("//span[text()='Next']").click();
			}
		} else if (monthToSelect < presentMonth) {
			for (int i = 0; i < presentMonth - monthToSelect; i++) {
				driver.findElementByXPath("//span[text()='Prev']").click();
			}
		}
		
	  

		driver.findElementByLinkText(splitDate[0]).click();
	}
}

