package interviewPrograms;

import org.apache.commons.math3.stat.descriptive.summary.Sum;

public class OverridingClass2 extends OverridingClass1 {

	public void sum(double d,double e) {
		System.out.println(d+e);
	}
	
	public static void main(String[] args) {
		OverridingClass2 obj = new OverridingClass2();
		obj.sum(5.7, 5.6);

	}

}
