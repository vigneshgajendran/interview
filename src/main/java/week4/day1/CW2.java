package week4.day1;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class CW2 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementByLinkText("Contact Us").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> ls = new ArrayList<>();
		ls.addAll(windowHandles);
		driver.switchTo().window(ls.get(1));
		System.out.println(driver.getTitle());
		System.out.println(ls.get(0));
		System.out.println(ls.get(1));
		//driver.switchTo().window(ls.get(0));
		//driver.close();
	}

}
