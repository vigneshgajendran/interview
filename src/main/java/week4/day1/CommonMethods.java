package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethods {


	public void allMethods() throws IOException {
		//Launch the browser and get the URL and Maximize
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
        driver.get("http://leaftaps.com/opentaps/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        
        
        //Select a value from a dropdown using visible text
        WebElement dd = driver.findElementById("using");
	    Select sel = new Select(dd);
	    //Select dropdown using selectByVisibleText
	    sel.selectByVisibleText("");
	    //Select dropdown using selectByValue
	    sel.selectByValue("");
	    //Select dropdown using selectByIndex
	    sel.selectByIndex(0);
	    //get list of options in the dropdown
	    List<WebElement> options = sel.getOptions();
	    options.size();
	    
	    //all Verifications
	    //Get text if a DOM element text
	    String elementText = driver.findElementById("").getText();
	    String title = driver.getTitle();
	    String currentUrl = driver.getCurrentUrl();
	    boolean selected = driver.findElementById("").isSelected();
	    boolean displayed = driver.findElementById("").isDisplayed();
	    boolean enabled = driver.findElementById("").isEnabled();
	 
	    //Switch to windows
	    //Get the list of references of tabs in the browser in a Set
	    Set<String> setTabs = driver.getWindowHandles();
	    
	    //Get list of tabs - Options 1
	    /*List<String> listTabs = new ArrayList<>();
	    listTabs.addAll(setTabs);
	    driver.switchTo().window(listTabs.get(1));*/
	    
	    //Get list of tabs - Options 2
	    //Move the values from Set to the List
	    List<String> listTabs = new ArrayList<>(setTabs);
	    //Move to the specified window
	    driver.switchTo().window(listTabs.get(1));
	  
	    //Take SnapShot
	    // Takes a snap and stores in the clipboard, it just holds the snap file type is not mentioned 'OutputType.FILE' as it can be anything
	    File src = driver.getScreenshotAs(OutputType.FILE);
	    // Save the file in the specified path with type mentioned
	    File des = new File("./snaps/img.png");
	    //Move the file from source to Destination
	    FileUtils.copyFile(src, des);
	    
	    //Alerts
	    Alert alert = driver.switchTo().alert();
	    //Alert Accept
	    alert.accept();
	    //Alert Dismiss
	    alert.dismiss();
	    //Alert SendKeys
	    alert.sendKeys("");
	    //Alert Get Text
	    alert.getText();
	    
	    //Frames
	    //Using Index
	    driver.switchTo().frame(0);
	    //Using Name or ID
	    driver.switchTo().frame("");	    
	    //Using Frame Element 
	    WebElement frameWebelement = driver.findElementById("");
	    driver.switchTo().frame(frameWebelement);
	    //Move back to Base html
	    driver.switchTo().defaultContent();
	    //When Nexted Frames is there, to move back to previous parent Frame
	    driver.switchTo().parentFrame();
	    
	    //Accesing the values in the table
	    WebElement table1 = driver.findElementById("");
	    //Get the list of rows of table1
	    List<WebElement> rows = table1.findElements(By.tagName("tr"));
	    //Assign row 4 to a Web Element
	    WebElement row4 = rows.get(3);
	    //Get list of Coloums of row 1
	    List<WebElement> cols = row4.findElements(By.tagName("tr"));
	    //Access and read the 3rd Column of 4th row
	    cols.get(2).getText();
	    
	    //Wait
	    //Implicit wait
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	    //Explicit Wait
	    // 1 -> Thread Sleep
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    //WebDriver Wait
	    WebDriverWait wait = new WebDriverWait(driver, 10);
	    //Element for which WebDriver Wait has to be applied
	    WebElement element = driver.findElementById("");
	    //Mention for what condition the wait has to be applied
	    wait.until(ExpectedConditions.elementToBeClickable(element));
	    
	    
	    //Locators
	    driver.findElementById("").clear();
	    driver.findElementById("").click();
	    driver.findElementByXPath("").click();
	    driver.findElementByClassName("").getAttribute("");
	    driver.findElementByLinkText("").getText();
	    driver.findElementByCssSelector("").getCssValue("");
	    WebElement elementName = driver.findElementByClassName("");
	}

}
