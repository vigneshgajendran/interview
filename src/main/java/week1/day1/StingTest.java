package week1.day1;

import java.util.Scanner;

public class StingTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StingTest test = new StingTest();

		test.countA();
		test.convertHello();
		test.sumOdd();
		test.SortInt();
	}

	public void countA() {
		String text = "have a wonderful year ahead and best of luck";

		char[] txt = text.toCharArray();
		int count = 0;
		System.out.println(txt);
		for (int i = 0; i < txt.length; i++) {
			if (txt[i] == 'a')
				count++;
		}
		System.out.println("number of 'a' in the string is :" + count);
	}

	public void convertHello() {
		String txt = "hello";

		int index = txt.indexOf('e');
		System.out.println("index of 'e' is " + index);

		char[] charArray = txt.toCharArray();

		charArray[index] = 'E';
		for (int i = 0; i < charArray.length; i++)
			System.out.print(charArray[i]);

	}

	public void sumOdd() {
		String nm = "14783";

		int sum = 0;
		for (int i = 0; i <= nm.length() - 1; i++) {
			int num = Integer.parseInt(nm.charAt(i) + "");

			if (num % 2 != 0) {
				sum = sum + num;
			}

		}
		System.out.println("\n"+"Sum of odd number is :"+sum);
	}

//	public void SortInt() {
//		int[] number = new int[] { 4, 1, 7, 6, 0 };
//		int temp = 0,len=number.length - 1;
//		for (int i = 0; i < len; i++){			
//		  if(number[i]>number[len]) {
//				temp = number[len];
//				number[len] = number[i];
//				number[i] = temp;
//			}
//		}
//
//		for (int i = 0; i < number.length; i++)
//			System.out.println(number[i]);
//	}

	public void SortInt() {
		int[] number = new int[] { 4, 1, 7, 6, 0 };
		int temp = 0, len = number.length;
		System.out.println("Integer array before sorting : ");
		
		for (int i = 0; i < number.length; i++)			
			System.out.print(number[i]);
		for (int i = 0; i < len; i++)
			for (int j = i + 1; j < len; j++) {
				if (number[i] > number[j]) {
					temp = number[j];
					number[j] = number[i];
					number[i] = temp;
				}
			}
		System.out.println("\n"+"Integer array after sorting : ");
		for (int i = 0; i < number.length; i++)
			System.out.print(number[i]);
	}
}
