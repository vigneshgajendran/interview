package week1.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Multimap;

public class Mobile {
	public boolean isMobileOn = true;

	public void makeCall() {
		System.out.println("Make Call");
	}

	public void getSms() {
		System.out.println("Get SMS");
	}

	public String gpsenabled() {
		return "GPS Enabled";
	}

	public String isMobileStatus() {
		if (isMobileOn) {
			return "Mobile is on";
		} else
			return "Mobile is Off";
	}

	public String getColor(String person) {
		switch (person) {
		case "father":
			return "black";
		case "mother":
			return "green";
		case "mine":
			return "blue";
		default:
			return "black";
		}
	}

	public void greatestOfThreeNumbers() {
		int a = 15, b = 15, c = 13;
		if (a > b) {
			if (a > c)
				System.out.println("greatest number is a " + a);
			else if (b > c)
				System.out.println("greatest number is b " + b);
			else
				System.out.println("greatest number is c " + c);
		}
	}

	// 9  8   6 
	public void secondSmallestNumber(int number1, int number2, int number3) {
		if (number1 > number2 && number1 > number3) {
			if (number2 > number3)
				System.out.println("Second smallest number is :" + number2);
			else
				System.out.println("Second smallest number is :" + number3);
		}

		else if (number2 > number1 && number2 > number3) {
			if (number1 > number3)

				System.out.println("Second smallest is " + number1);

			else
				System.out.println("Second smallest is " + number3);
		}
		
		else if (number3 > number1 && number3 > number2) {
			if (number2 > number3)

				System.out.println("Second smallest is " + number2);

			else
				System.out.println("Second smallest is " + number3);
		}

	}

	public void fibonacci() {
		int a = 0, b = 1, c;
		do {
			c = a + b;
			a = c;
			b = a;
			System.out.println("factorial of 12 is :" + c);
		} while (c <= 12);
	}

	public void factorial() {
		int x, fact = 1;
		for (x = 5; x == 1; x--) {
			fact = x * fact;
			System.out.println(fact);
		}
	}

	public int sum(int n) {
		int sum = 0;
		do {
			sum = sum + n;
			n--;
		} while (n >= 1);
		return sum;
	}

	public int countOdd(int n) {
		int count = 0;
		while (n > 0) {
			if ((n % 2) != 0)
				count++;
			n--;
		}
		return count;
	}

	public void table(int n) {
		for (int i = 1; i <= 10; i++)
			System.out.println(n + " * " + i + " = " + n * i);
	}

	public void prime() {
		for (int number = 1; number <= 50; number++)
			for (int denominator = 1; denominator <= 50; denominator++) {
				int remainder = number % denominator;
				if (remainder == 0) {
					System.out.println(number + " a prime number");
				}

			}
	}
}