package week12day1;

import java.util.*;

public class HomeWork2 {

	public static void main(String[] args) {

		System.out.println(
				"Create a list of employee name and remove all the names contain 'd' and print it sorting order");

		ArrayList<String> names = new ArrayList<>();

		names.add("Dilip");
		names.add("Peter");
		names.add("Dilip");
		names.add("Matt");
		names.add("Philip");
		int indexOf = 0;
		for (String name : names) {
			System.out.println(name);
			if (name.contains("D")) {
				indexOf = name.indexOf(name);
			}
		}

		names.remove(indexOf);
		System.out.println(names);
	}
}
