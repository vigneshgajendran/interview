package week12day1;

import java.util.*;

public class ArrayListImplementation {

	public static void main(String[] args) {

		List<String> name = new ArrayList<String>();

		name.add("BMW");
		name.add("Audi");
		name.add("Hyundai");
		name.add("Suzuki");
		name.add("Ford");
		name.add("Ford");

		System.out.println("size of the array");
		System.out.println(name.size());
		System.out.println("Elements in the list");
		for (String each : name) {
			System.out.println(each);
		}
		System.out.println("-------------");
		System.out.println("gets the element in the index '1'");
		System.out.println(name.get(1));
		System.out.println("-------------");
		System.out.println("remove the element in the index '1'");
		name.remove(1);
		System.out.println("-------------");
		System.out.println("size of the array");
		System.out.println(name.size());
		System.out.println("Elements in the list");
		for (String each : name) {
			System.out.println(each);
		}
		System.out.println("-------------");
		System.out.println("Checks whether audi string is available, should be false since removed previously");
		System.out.println(name.contains("Audi"));
		System.out.println("-------------");
		System.out.println("Checks whether 'Hyundai' is available in the list, should be true");
		System.out.println(name.contains("Hyundai"));
		System.out.println("-------------");
		System.out.println("checks whether list is empty - should be false");
		System.out.println(name.isEmpty());
		System.out.println("-------------");
		System.out.println("clear all the elements in the list");
		name.clear();
		System.out.println("-------------");
		System.out.println("checks whether list is empty - should be true");
		System.out.println(name.isEmpty());
		System.out.println("-------------");	

	
	}

}
