package testcase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://leaftaps.com/opentaps/");

		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();

		driver.findElementByXPath("(//span[@class='x-tab-strip-inner'])[3]").click();

		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("testgts@test.com");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[7]").click();
		/*
		 * WebElement table =
		 * driver.findElementByXPath("(//table[@class='x-grid3-row-table'])[1]");
		 * List<WebElement> rows = table.findElements(By.tagName("tr")); WebElement row0
		 * = rows.get(0); List<WebElement> col1 = row0.findElements(By.tagName("td"));
		 * String name = col1.get(1).getText(); System.out.println(name);
		 */

		String duplicateName = driver
				.findElementByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a").getText();
		driver.findElementByXPath("//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a").click();

		driver.findElementByXPath("//a[@class='subMenuButton']").click();
		driver.getTitle().equals("Duplicate Lead");
		driver.findElementByClassName("smallSubmit").click();
		String DuplicateCompanyText = driver.findElementById("viewLead_companyName_sp").getText();

		if (DuplicateCompanyText.equals(duplicateName))
			System.out.println("Confirmed");
		else
			System.out.println("Duplicate name is not correct");

		driver.close();

	}

}
