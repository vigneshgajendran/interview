package testcase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://leaftaps.com/opentaps/");

		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//div[@class='x-form-item x-tab-item']/following::input").sendKeys("first");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("TimeOut Exception");
			}
		
		
		driver.findElementByXPath("(//div[@class=\"x-grid3-cell-inner x-grid3-col-partyId\"]//a[@class=\"linktext\"])[1]").click();
		System.out.println(driver.getTitle());
		driver.findElementByXPath("//div[@class='frameSectionExtra']//preceding-sibling::a[text()='Edit']").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("New Company");
		driver.findElementByName("submitButton").click();
		String newCompanyID = driver.findElementById("viewLead_companyName_sp").getText();
		if (newCompanyID.contains("New Company"))
			System.out.println("New company contact is updated");
		driver.close();

	}

}
