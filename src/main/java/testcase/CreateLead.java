package testcase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {
		public static void main(String[] args) throws InterruptedException {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);

			driver.get("http://leaftaps.com/opentaps/");

			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByPartialLinkText("Create Lead").click();
//			if(driver.findElementByClassName("frameSectionBody").isDisplayed());{
//				driver.findElementById("username").sendKeys("DemoSalesManager");
//				driver.findElementById("password").sendKeys("crmsfa");
//				driver.findElementByClassName("decorativeSubmit").click();
//						
//			}
			driver.findElementById("createLeadForm_companyName").sendKeys("Company");
			driver.findElementById("createLeadForm_firstName").sendKeys("First");
			driver.findElementById("createLeadForm_lastName").sendKeys("Last");
			// First

			WebElement sourceElement = driver.findElementById("createLeadForm_dataSourceId");
			Select selectSource = new Select(sourceElement);
			selectSource.selectByVisibleText("Employee");
			// Second
			WebElement sourceMar = driver.findElementById("createLeadForm_marketingCampaignId");
			Select selectMarket = new Select(sourceMar);
			selectMarket.selectByValue("CATRQ_CARNDRIVER");
			driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Local First");
			driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Local Last");
			driver.findElementById("createLeadForm_personalTitle").sendKeys("Salutation");
			driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Title");
			driver.findElementById("createLeadForm_departmentName").sendKeys("Department");
			driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
			// Select Currency
			WebElement sourceCurr = driver.findElementById("createLeadForm_currencyUomId");
			Select sourceCurrency = new Select(sourceCurr);
			List<WebElement> countCurr = sourceCurrency.getOptions();
			sourceCurrency.selectByIndex(countCurr.size() - 2);
			// Select Industry
			WebElement sourceIndus = driver.findElementById("createLeadForm_industryEnumId");
			Select selectIndustry = new Select(sourceIndus);
			List<WebElement> listIndustry = selectIndustry.getOptions();
			selectIndustry.selectByIndex(listIndustry.size() - 1);
			driver.findElementById("createLeadForm_numberEmployees").sendKeys("5");
			// Select Ownership
			WebElement sourceOwner = driver.findElementById("createLeadForm_ownershipEnumId");
			Select selectOwnership = new Select(sourceOwner);
			selectOwnership.selectByVisibleText("Partnership");
			driver.findElementById("createLeadForm_sicCode").sendKeys("12345");
			driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Ticker Symbol");
			driver.findElementById("createLeadForm_description").sendKeys("Description");
			driver.findElementById("createLeadForm_importantNote").sendKeys("Description");
			// Contact Information
			driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("Area code");
			driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("phone number");
			driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("1111");
			driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Person to ask for");
			driver.findElementById("createLeadForm_primaryEmail").sendKeys("aaa@gmail.com");
			driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("Web URL");
			// Primary Address
			driver.findElementById("createLeadForm_generalToName").sendKeys("To Name");
			driver.findElementById("createLeadForm_generalAttnName").sendKeys("Attention Name");
			driver.findElementById("createLeadForm_generalAddress1").sendKeys("Addr Line 1");
			driver.findElementById("createLeadForm_generalAddress2").sendKeys("Addr Line 2");
			driver.findElementById("createLeadForm_generalCity").sendKeys("City");
			// Select Country
			WebElement sourceCn = driver.findElementById("createLeadForm_generalCountryGeoId");
			Select selectCountry = new Select(sourceCn);
			selectCountry.selectByValue("IND");

			// Select State
			WebElement sourceSt = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
			Select selectState = new Select(sourceSt);
			selectState.selectByVisibleText("TAMILNADU");
			Thread.sleep(5000);

			driver.findElementById("createLeadForm_generalPostalCode").sendKeys("999");
			driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("999");

//			for (WebElement eachIndustry : listIndustry) {
//				if (eachIndustry.getText().startsWith("M"))
//					System.out.println(eachIndustry.getText());
//			}

			 driver.findElementByName("submitButton").click();
			 Thread.sleep(1000);
			 String companyID = driver.findElementById("viewLead_companyName_sp").getText();
			  
			 
			// driver.findElementByLinkText("Logout").click();
			// driver.close();


	}

}
