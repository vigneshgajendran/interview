package homeWork.week1;

import java.util.Scanner;

public class CodeChallenge6 {

	public void factorial(int input) {
		int x, fact = 1;
		for (x = input; x >= 1; x--) {
			fact = x * fact;
		}
		System.out.println(fact);
	}

	public static void main(String[] args) {
		CodeChallenge6 cc = new CodeChallenge6();
		System.out.println("Enter the number to get factorial");
		Scanner inp = new Scanner(System.in);
		int input = inp.nextInt();
		
		cc.factorial(input);
	}

}

