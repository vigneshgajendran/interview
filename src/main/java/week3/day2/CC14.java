package week3.day2;

import java.util.Scanner;

public class CC14 {

	public static void main(String[] args) {

		System.out.println("Enter the number of rows");
		Scanner no = new Scanner(System.in);
		int n = no.nextInt();
		int k = 1;
		for (int i = 0; i <= n; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(k++);
			}
			System.out.println();
		}

	}
}
