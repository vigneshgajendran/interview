package week3.day2;

public class CC15 {
	public static void main(String[] args) {
		int[] intArray = { 95, 24, 65, -129, 55, 72, 89, 43, 25 };
		// int[] intArray = { 95, 24, 65 };

		for (int i = 0; i < intArray.length; i++) {
			for (int j = 0; j < intArray.length; j++) {
				if (intArray[i] > intArray[j]) {
					int temp = intArray[j];
					intArray[j] = intArray[i];
					intArray[i] = temp;
				}
			}
		}

		System.out.println(intArray[1]);

	}

}
