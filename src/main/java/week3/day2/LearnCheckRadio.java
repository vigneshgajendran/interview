package week3.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnCheckRadio {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://jqueryui.com/checkboxradio/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().frame(0);
		driver.findElementByXPath("//div[@class='widget']/fieldset//following-sibling::label[contains(text(),'London')]/span").click();
		driver.findElementByXPath("//div[@class='widget']/fieldset//following-sibling::label[contains(text(),'2 Star')]/span").click();
		driver.findElementByXPath("//div[@class='widget']/fieldset//following-sibling::label[contains(text(),'3 Star')]/span").click();
	}
	
}
