package week3.day2;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {

		System.out.println("Enter the year number");
		Scanner yr = new Scanner(System.in);
		int x = yr.nextInt();
		String result = ((x % 4 == 0) && (x % 100 != 0)) ? " is a Palindrome Number"
				: (x % 400 == 0) ? " is a Palindrome Number" : " is Not a palindrome number";
		System.out.println(x + result);
				
	}
}
